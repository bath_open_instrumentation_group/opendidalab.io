---
layout: home
title: Open digital diagnostics for better healthcare in Africa
sitemap: true
---
# Open digital diagnostics <br> for better healthcare in Africa

Modern digital diagnostic technology guides medical treatment in most wealthy countries, but is often unavailable in low and middle-income countries.  This is not simply a question of cost; unreliable supply chains, vendor lock-in, lack of maintenance infrastructure, and design that ignores the different challenges of underresourced healthcare systems mean donated medical equipment often quickly becomes idle or broken.

We are a network of engineers, scientists, clinicians, researchers, and entrepreneurs based over 10 countries in Africa, the UK, the US, and Peru, seeking to use open source hardware to make access to modern digital diagnostic technology more equitable.  For more detail, see our [aims and vision] page.

This network is funded as part of the UK's [Global Challenges Research Fund][GCRF] ([GCRF]), an initiative that funds academic research for the benefit of low and middle income countries by researchers in the UK and those countries.  It is part of the [DIDA] programme, currently in Phase 1 running from 1 May 2020 for one year.  

Our membership currently includes partners in Tanzania, Ghana, Cameroon, Kenya, Uganda, Ethiopia, South Africa, Peru, USA, and the UK.  We meet approximately monthly by teleconference for talks and discussions, with pilot projects and scoping exercises happening between meetings.  We are actively building the network over this first year, and would welcome [contact] from interested clinicians, researchers, entrepreneurs, policymakers, regulators, and other interested parties.

[GCRF]: https://www.ukri.org/our-work/collaborating-internationally/global-challenges-research-fund/
[DIDA]: https://webarchive.nationalarchives.gov.uk/20200923112731/https://www.ukri.org/research/global-challenges-research-fund/gcrf-digital-innovation-for-development-in-africa-dida/
[contact]: # "TODO: make a contact page"
[aims and vision]: # "TODO: aims and vision page"