---
layout: page
title: Vision and Aims
permalink: /vision/
sitemap: true
---

# Network aims and vision

Well-funded medical labs are highly automated; detailed digital images and results are recorded by computerised instruments.  This improves throughput, quality control, and record-keeping, and would enable training and telemedicine for rural contexts where an expert technician is not available.  Currently, automated diagnostic devices are expensive, manufactured in rich countries.  Most of the Global South is left behind, not only because of the high up-front cost, but because equipment cannot be *maintained* locally ([WHO 1997]), due to proprietary technologies and conservative regulations.  This network will help bring digital diagnostics to laboratories and clinics across Africa, in sustainable and responsible partnership with local clinicians and engineers.  Our vision is of **integrated, modern, digital healthcare** supported by a **thriving ecosystem of biomedical engineers, developers, and local businesses**.  

The crux of our approach is the use of "open source hardware", where designs for easily replicated, high-quality diagnostic tools are shared under a license permitting their use, sale, and modification.  Crucially, open source instruments share not only technical know-how, but also *ownership* of innovations. Using this as a model for technology transfer in Africa could be transformative in healthcare innovation. We will build a network of researchers, clinicians, and entrepreneurs between Africa and the UK, preparing the ground for a large-scale empirical study into the use of open source hardware, and strong engagement with business and government.

To reach our vision we must build a body of knowledge and skills that enable a new generation of medical instruments that can be repaired and customised without relying on a handful of rich countries.  **Our aim is to test the potential of open-source hardware as a new business model to establish and scale digital diagnostic solutions in LMICs,** using the [OpenFlexure Microscope] as a case study.

## Objectives for Phase One

* **Understand and map out the process** required to implement the [OpenFlexure Microscope] as a diagnostic tool in several of our member countries, identify "technology champions" to lead implementation in each location, and build agile, local implementation networks

* **Explore the specific factors that help or hinder the uptake** of open source diagnostics, to understand the different approaches needed in each place, and reveal the common themes that should inform national and international policy

* **Identify other open source projects, and unmet needs** for digital diagnostic technology, that could benefit from what we have learned, and be developed in Phase Two

* **Connect with new and existing initiatives to build capacity** in biomedical engineering in Africa, empowering them to use and create open source hardware that can rapidly diffuse across the continent


The [OpenFlexure Microscope] (OFM) is a high-quality automated microscope, developed at Bath as open source hardware.  This 3D printed instrument is relatively mature, but not yet certified for medical use.  Our network will both facilitate the implementation of the OFM as a healthcare intervention, and learn from studying that journey to enable other technologies to scale more rapidly across Africa.  We will also identify and include other technologies that can benefit from a similar approach - for example, retinal imaging, or basic triage diagnostics that integrate with patient management systems.  Digital optical imaging in healthcare spans a range of applications from malaria diagnostics using blood smears to opthalmology, and many of the components, self-tests, and regulatory hurdles are very similar.  This means there is much to gain from exchange of knowledge in both directions with other projects.

Determining the correct research questions to ask in Phase Two is an important part of Phase One, but we have collected a number of "key questions" here as a starting point.  All of these apply to the OFM, but are of interest to a broad range of different technologies:

* What is the best way to build capacity in engineering, coding, and quality-control?
* How can we equitably share production and ownership of digital healthcare technologies, while maintaining quality and regulatory compliance?
* How do we ensure machine learning is used responsibly in medical applications?
* What are the factors that determine whether innovations can scale, and how do we best replicate success between countries?

During this first year we will be looking ahead to future years, where we hope not only to answer our key questions, but to see these answers implemented.  Our long-term goals are **to see the first in-vitro diagnostic device certified and manufactured in Tanzania by 2023**, and to **promote the formation of similar clusters in other countries in the network**.


[WHO97]: https://apps.who.int/iris/handle/10665/70806 "World Health Organisation WHO/ARA/97.3 (1997)"
[OpenFlexure Microscope]: https://openflexure.org/projects/microscope "The OpenFlexure Microscope website"