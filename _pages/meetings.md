---
layout: page
title: Meetings
permalink: /meetings/
sitemap: true
---

Our meetings typically take place from 1pm-4pm UK time, and comprise two or three short talks interspersed with longer discussion sessions.  We generally meet on Zoom, with 20-30 participants at each meeting.  Meetings are advertised via the project's mailing list, to join this list please visit the [contact page](/contact/).